# Instructions

### Setup project

1. Install nodeJS latest version 

2. Clone project  
```code
git clone https://mzerara@bitbucket.org/mzerara/var-frontend.git
```

3. Install project dependencies 
```code
npm install
```

### Builds 

1. Run development build
```code
npm run build 
```

2. Run production build
```code
npm run prod
```

### Launch project 

1. Run on development mode
```code
npm run dev 
```
