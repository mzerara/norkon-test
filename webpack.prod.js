// Dependencies
const merge = require("webpack-merge");
const common = require("./webpack.common");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ZipPlugin = require("zip-webpack-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");


// Webpack configs
module.exports = merge(common.configs, {

    mode: "production",

    // devtool: "source-map",

    output: {
        path: common.folders.dist,
        filename: "js/[name].bundle.[contenthash].js",
        chunkFilename: "js/[name].chunk.[contenthash].js"
    },

    module: {

        rules: [

            {
                test: /\.css$/,
                use: [

                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: true
                        }
                    }

                ]
            },

            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            }

        ]

    },

    plugins: [

        new HtmlWebpackPlugin({
            template: path.resolve(common.folders.src, "index.html"),
            minify: {
                html5: true,
                minifyCSS: true,
                minifyJS: true,
                collapseWhitespace: true,
                removeComments: true,
                keepClosingSlash: true
            }
        }),

        new MiniCssExtractPlugin({
            filename: "css/[name].styles.[contenthash].css",
            chunkFilename: "css/[name].chunk.[contenthash].css"
        }),

        new ZipPlugin({
            path: "zip",
            filename: "app.zip"
        })

    ],

    optimization: {

        minimizer: [

            new OptimizeCssAssetsPlugin({
                cssProcessorPluginOptions: {
                    preset: [
                        "default", {
                            discardComments: {
                                removeAll: true
                            }
                        }
                    ]
                },
                canPrint: true
            }),

            new TerserPlugin({
                cache: false,
                parallel: true,
                sourceMap: false,
                extractComments: false,
                terserOptions: {
                    output: {
                        comments: false
                    }
                }
            })

        ]

    }

});