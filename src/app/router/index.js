// Dependencies
import Vue from "vue";
import VueRouter from "vue-router";
import App from "../App.vue";


// Plugins 
Vue.use(VueRouter);


// Vue-router
export default new VueRouter({

    mode: "history",

    routes: [

        {
            path: "/",
            name: "index",
            component: App
        },
        {
            path: "*",
            redirect: {
                name: "index"
            }
        }

    ]

});