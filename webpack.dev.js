// Dependencies
const merge = require("webpack-merge");
const common = require("./webpack.common");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WriteFilePlugin = require("write-file-webpack-plugin");


// Webpack configs
module.exports = merge(common.configs, {

    mode: "development",

    devtool: "cheap-module-eval-source-map",

    output: {
        path: common.folders.dist,
        filename: "js/[name].bundle.js",
        chunkFilename: "js/[name].chunk.js"
    },

    module: {
        rules: [

            {
                enforce: "pre",
                test: /\.(js|vue)$/,
                exclude: /node_modules/,
                loader: "eslint-loader"
            },

            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    "css-loader"
                ]
            },

            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            }

        ]
    },

    plugins: [

        new HtmlWebpackPlugin({
            template: path.resolve(common.folders.src, "index.html")
        }),

        new WriteFilePlugin()

    ],

    devServer: {
        contentBase: common.folders.dist,
        historyApiFallback: true,
        port: 8080,
        open: true
    }

});