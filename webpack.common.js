// Dependencies
const path = require("path");
const ProgressBarPlugin = require("progress-bar-webpack-plugin");
const chalk = require("chalk");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin");


// Directories
const srcFolder = path.resolve(__dirname, "src");
const distFolder = path.resolve(__dirname, "dist");
exports.folders = {
    src: srcFolder,
    dist: distFolder
};


// Webpack configs
exports.configs = {

    entry: {
        app: path.resolve(srcFolder, "app/app.js")
    },

    module: {

        rules: [

            {
                test: /\.vue$/,
                loader: "vue-loader"
            }

        ]

    },

    plugins: [

        new ProgressBarPlugin({
            format: "  build [:bar] " + chalk.green.bold(":percent") + " (:elapsed seconds)",
            clear: false
        }),

        new CleanWebpackPlugin(),

        new VueLoaderPlugin()

    ]

};